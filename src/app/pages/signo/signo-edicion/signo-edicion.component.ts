import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { Paciente } from 'src/app/_model/paciente';
import { Signos } from 'src/app/_model/signo';
import { PacienteService } from 'src/app/_service/paciente.service';
import { SignoService } from 'src/app/_service/signo.service';

@Component({
  selector: 'app-signo-edicion',
  templateUrl: './signo-edicion.component.html',
  styleUrls: ['./signo-edicion.component.css']
})
export class SignoEdicionComponent implements OnInit {

  pacientes$: Observable<Paciente[]>;
  form: FormGroup;
  pacienteSeleccionado :any;
  fechaSeleccionada: Date = new Date();
  myControlPaciente: FormControl = new FormControl();
  signo: Signos;
  id: number;
  edicion: boolean = false;

  constructor(
    private pacienteService: PacienteService,
    private route: ActivatedRoute,
    private signoService: SignoService,
    private router: Router
  ) { }

  ngOnInit(): void {

    this.signo = new Signos();

    this.form = new FormGroup({
      'id': new FormControl(0),
      'paciente': this.myControlPaciente,
      'fecha': new FormControl(new Date()),
      'temperatura': new FormControl(''),
      'pulso': new FormControl(''),
      'ritmo': new FormControl(''),
    });
    this.fechaSeleccionada = new Date();
    this.fechaSeleccionada.setHours(0);
    this.fechaSeleccionada.setMinutes(0);
    this.fechaSeleccionada.setSeconds(0);
    this.fechaSeleccionada.setMilliseconds(0);
    
    this.pacientes$ = this.pacienteService.listar();

    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = params['id'] != null;
      this.initForm();
    });
  }

  initForm() {
    if (this.edicion) {
      this.signoService.listarPorId(this.id).subscribe(data => {

        this.form = new FormGroup({
          'id': new FormControl(data.idSigno),
          'paciente': new FormControl(""),
          'fecha': new FormControl(new Date(data.fecha)),
          'temperatura': new FormControl(data.temperatura),
          'pulso': new FormControl(data.pulso),
          'ritmo': new FormControl(data.ritmo),
        });

        this.form.controls['paciente'].setValue(data.paciente);

      });
    }
  }

  comparePaciente(object1: any, object2: any) {
    return object1 && object2 && object1.idPaciente == object2.idPaciente;
}

  operar() {

    this.signo.idSigno = this.form.value['id'];
    this.signo.paciente = this.form.value['paciente'];
    this.signo.fecha = moment(this.form.value['fecha']).format('YYYY-MM-DDTHH:mm:ss');
    this.signo.temperatura = this.form.value['temperatura'];
    this.signo.pulso = this.form.value['pulso'];
    this.signo.ritmo = this.form.value['ritmo'];

    if (this.signo != null && this.signo.idSigno > 0) {
      this.signoService.modificar(this.signo).pipe(switchMap(() => {
        return this.signoService.listar();
      })).subscribe(data => {
        this.signoService.setsignosCambio(data);
        this.signoService.setMensajeCambio("Se modificó");
      });

    } else {
      this.signoService.registrar(this.signo).subscribe(data => {
        this.signoService.listar().subscribe(signo => {
          this.signoService.setsignosCambio(signo);
          this.signoService.setMensajeCambio("Se registró");
        });
      });
    }

    this.router.navigate(['/pages/signo']);

  }
}
